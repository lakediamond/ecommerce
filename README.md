# E-commerce

- **Front**: nodejs shop which connect to online moltin.com to list product
- **Invoice**: Configured moltin.com webhook will POST order info to invoice URL to create PDF invoice and send it to user via mailjet.

Browse our e-commerce: http://shop.lakediamond.ch

## Deploy locally with Docker

1. Run 
```
MOLTIN_ID="xx" \
MOLTIN_SECRET="xx" \
PAYMENT_CONTRACT_ADDRESS="xx" \
PAYMENT_ACCOUNT_ADDRESS="xx" \
REACT_APP_LD_LINK_PREFIX="hxx" \
MAILJET_APIKEY_PUBLIC="xx" \
MAILJET_APIKEY_PRIVATE=xx" \
MAILJET_SENDER_EMAIL="xx" \
TAG=develop \
  docker-compose up -d --build
```
2. Access your app on with http://localhost:5000

## Local development without docker

```bash
git clone https://gitlab.com/lakediamond/ecommerce.git
cd ecommerce
yarn # or npm install
yarn start # or npm start
```

## Invoicing
```sh
cd invoicing
yarn
yarn start
```

### REQUIRED ENV VARS
```bash
MJ_APIKEY_PUBLIC
MJ_APIKEY_PRIVATE
MJ_SENDER_EMAIL
MJ_SENDER_NAME
CLIENT_ID              # MOLTIN_CLIENT ID
CLIENT_SECRET          # MOLTIN_SECRET KEY
```
