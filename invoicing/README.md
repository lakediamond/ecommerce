# Moltin Invoicing

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

This basic application, once running will:

* Listen for moltin orders being created, and automatically generate a PDF invoice for each order.
* Send an automated email to the customer with their invoice.

## Setup

You'll need to deploy the app and give it the required config vars.

## Webhook

Once you have deployed the app, you'll need to create a moltin webhook and give it your apps url in the configuration appended with the `/orders` route. You can create a moltin webhook like so:

```
     -H "Authorization: a5a149059dcdbd640006b1319d17cb1809ab1325" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d $'{
  "data": {
    "configuration": {
      "url": "http://5e082d80.ngrok.io/orders"
    },
    "observes": [
      "order.created"
    ],
    "enabled": true,
    "type": "integration",
    "name": "invoicing",
    "integration_type": "webhook"
  }
}'
```

## Email

If using the email feature, you'll have to make sure your gmail account has `less secure apps` turned on. More info here: https://myaccount.google.com/lesssecureapps

## S3

This app also provides the ability to save PDF invoices to an S3 bucket. _The functionality is a WIP_.

## CONFIG VARS

MJ_APIKEY_PUBLIC=120ffeb1bf0318661447411eeb828078
MJ_APIKEY_PRIVATE=59c2e3e30923a227cde17112b40161e1
MJ_SENDER_EMAIL=ops@lakediamond.ch
MJ_SENDER_NAME=No Reply
CLIENT_ID=JQlLW8agLhwYafD9pOlrk2zc9fu3tzFskhWHBIJjxX
CLIENT_SECRET=zPH5a53cyEiOewOvxXupazgwoTMjd5Js0tZ2tGRCKV
PORT=3000

## RUN

setup Moltin webhook to order.created to http://INVOICE-GEBERATOR-HOST:3000/orders

`$ node start`
