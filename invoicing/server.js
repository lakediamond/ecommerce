const express = require('express');
const bodyParser = require('body-parser');
const s3Functions = require('./upload.js');
const invoiceFunctions = require('./invoice.js');
const moltin = require('@moltin/sdk');
const Moltin = moltin.gateway({
  client_id: process.env.CLIENT_ID || 'JQlLW8agLhwYafD9pOlrk2zc9fu3tzFskhWHBIJjxX',
  client_secret: process.env.CLIENT_SECRET || 'zPH5a53cyEiOewOvxXupazgwoTMjd5Js0tZ2tGRCKV'
});

const MailJet = require('./mailjet');

// require our env package
require('dotenv').config();

const app = express();
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded


const get_order_items = function(order_id, invoiceTo, invoiceCurrency, sendInvoiceTo, txHash) {
  Moltin.Orders.Items(order_id)
    .then(items => {
      const orderItems = items.data;
      if (orderItems.length < 1) {
        throw new Error('Order not found.')
      }

      const item = orderItems[0];
      console.log('INVOICE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
      console.log(JSON.stringify(item));
      console.log('INVOICE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');

      return {
        invoiceItem: {
          name: item.name,
          quantity: item.quantity,
          unit_cost: item.meta.display_price.with_tax.unit.amount,
          currency: item.meta.display_price.with_tax.unit.currency
        },
        emailItem: {
          orderId: order_id,
          quantity: item.quantity,
          itemName: item.name,
          amount: item.meta.display_price.with_tax.unit.formatted,
          txHash: txHash,
        }
      }
    })
    .then(({ invoiceItem, emailItem }) => {
      console.log('ITEMSINVOICE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
      console.log(invoiceItem);
      console.log('ITEMSINVOICE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      const filename = invoiceTo + '_' + order_id + '.pdf';
      invoiceFunctions.generateInvoice(
        filename,
        invoiceTo,
        invoiceCurrency,
        [invoiceItem],
        function() {
          console.log('Saved invoice to ' + filename);
          return MailJet.sendInvoice(filename, sendInvoiceTo, emailItem, function(err, data) {
            if (err) {
              console.log('ERROR>>>>>>>>>>>', err);
              return;
            }
            console.log('SENDING TO LD<<<<<<<<<<<<<<<<')
            MailJet.sendConfirmationToLD(invoiceTo, sendInvoiceTo, emailItem, function(err, data) {
              if (err) {
                console.log('ERROR>>>>>>>>>>>', err);
                return;
              }
            })
          });
        },
        function(error) {
          console.error(error);
        }
      );
    })

    .catch(error => {
      console.log(error);
    });
};

app.post('/orders', function(req, res) {
  console.log('ENTER>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
  pbody = JSON.parse(req.body.resources);
  const order_id = pbody.data.id;
  console.log('the parsed order id is: ' + order_id);
  const sendInvoiceTo = pbody.data.customer.email;
  const invoiceTo = pbody.data.customer.name;
  const invoiceCurrency = pbody.data.meta.display_price.with_tax.currency;
  const txHash = pbody.data.order_repayment_transaction_hash;
  const isComplete = pbody.data.status === 'complete';
  if (isComplete) {
    get_order_items(order_id, invoiceTo, invoiceCurrency, sendInvoiceTo, txHash);
  }
  return res.status(200).send();
});

app.get('/test', function(req, res) {
  res.send('app functioning successfully');
});

app.listen(process.env.PORT || 3000, function() {
  console.log('Example app listening on port 3000!');
});
