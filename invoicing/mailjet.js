const mailjet = require('node-mailjet');
const utils = require('utility');
var fs = require('fs');

const MJ_APIKEY_PUBLIC =
  process.env.MJ_APIKEY_PUBLIC || '';
const MJ_APIKEY_PRIVATE =
  process.env.MJ_APIKEY_PRIVATE || '';
const MJ_SENDER_EMAIL = process.env.MJ_SENDER_EMAIL || 'shop@lakediamond.ch';
const MJ_LD_EMAIL = process.env.MJ_LD_EMAIL || 'shop@lakediamond.ch';
const MJ_SENDER_NAME = process.env.MJ_SENDER_NAME || 'No Reply';

const mailjetClient = mailjet.connect(MJ_APIKEY_PUBLIC, MJ_APIKEY_PRIVATE, {
  version: 'v3.1'
});

const sendInvoice = (attachmentFilename, sendInvoiceTo, emailItem, cb) => {
  const { orderId, quantity, itemName, amount, txHash } = emailItem;
  const htmlMessage = `
  <p>Dear customer,</p>

  <p>Thank you for testing our LKD payment platform!</p>

  <p>This is only a demo payment processor and no product will be delivered.<br />
  You can track your payment using the history field of you metamask browser plugin.<p>

  <p>Find below for reference, your order details :

  <ul>
    <li>Order ID: ${orderId}</li>
    <li>Description: ${itemName}</li>
    <li>Quantity: ${quantity}</li>
    <li>Total amount: ${amount}</li>
    <li>TX hash: <a href="https://rinkeby.etherscan.io/tx/${txHash}">${txHash}</a></li>
  </ul>


  <p>Please feel free to contact us anytime if you want to know more about our products.</p>
  <p>Kind regards,</p>

  <p>The LakeDiamond team.</p>
  `;

  const attachmentFilePath = './' + attachmentFilename;
  // const subject = 'Your Invoice for order at LakeDiamond shop ✔'
  fs.readFile(attachmentFilePath, 'base64', function(
    err,
    contents
  ) {
    if (err) {
      cb(err);
      return;
    }
    const emailData = {
      Messages: [
        {
          From: {
            Email: MJ_SENDER_EMAIL,
            Name: MJ_SENDER_NAME
          },
          Subject: 'Your order at LakeDiamond shop',
          HtmlPart: htmlMessage,
          To: [{ Email: sendInvoiceTo }],
          Attachments: [
            {
              ContentType: 'application/pdf',
              Filename: attachmentFilename,
              Base64Content: contents,
            }
          ]
        }
      ]
    };

    mailjetClient
      .post('send')
      .request(emailData)
      .then(result => {
        console.log('SUCCESS>>>>>>>>>>>>>', sendInvoiceTo)
        cb(null, result.body);
        fs.unlink(attachmentFilePath);
      })
      .catch(err => {
        cb(err.statusCode);
      });
  });
};

const sendConfirmationToLD = (clientName, clientEmail, emailItem, cb) => {
  const { orderId, quantity, itemName, amount, txHash } = emailItem;
  const htmlMessage = `
  <p>A new demo order has been sent by:</p>

  <ul>
    <li>Client name: ${orderId}</li>
    <li>Client email: ${clientEmail}</li>
    <li>Order description: ${itemName}</li>
    <li>Unit: ${quantity}</li>
    <li>Total amount: ${amount}</li>
    <li>Payment method: "testnet" LKD</li>
    <li>Payment method: <a href="https://rinkeby.etherscan.io/tx/${txHash}">${txHash}</></li>
  </ul>

  <p>Kind regards,</p>

  <p>The Moltin shop</p>
  `;

  const emailData = {
    Messages: [
      {
        From: {
          Email: MJ_SENDER_EMAIL,
          Name: MJ_SENDER_NAME
        },
        Subject: `New demo payment from ${clientName}`,
        HtmlPart: htmlMessage,
        To: [{ Email: MJ_LD_EMAIL }],
      }
    ]
  };

  mailjetClient
    .post('send')
    .request(emailData)
    .then(result => {
      cb(null, result.body);
    })
    .catch(err => {
      cb(err.statusCode);
    });
};

module.exports = {
  sendInvoice,
  sendConfirmationToLD
};
