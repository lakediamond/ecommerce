FROM node:10.9-alpine

ARG REACT_APP_MOLTIN_CLIENT_ID
ARG REACT_APP_MOLTIN_CLIENT_SECRET
ARG REACT_APP_CONTRACT_ADDRESS
ARG REACT_APP_LD_LINK_PREFIX
ARG REACT_APP_PAYMENT_ACCOUNT_ADDRESS
ARG REACT_APP_BLOCKCHAIN_EXPLORER_BASE_URL

ENV REACT_APP_MOLTIN_CLIENT_ID=$REACT_APP_MOLTIN_CLIENT_ID
ENV REACT_APP_MOLTIN_CLIENT_SECRET=$REACT_APP_MOLTIN_CLIENT_SECRET
ENV REACT_APP_CONTRACT_ADDRESS=$REACT_APP_CONTRACT_ADDRESS
ENV REACT_APP_LD_LINK_PREFIX=$REACT_APP_LD_LINK_PREFIX
ENV REACT_APP_PAYMENT_ACCOUNT_ADDRESS=$REACT_APP_PAYMENT_ACCOUNT_ADDRESS
ENV REACT_APP_BLOCKCHAIN_EXPLORER_BASE_URL=$REACT_APP_BLOCKCHAIN_EXPLORER_BASE_URL

# The base node image sets a very verbose log level.
ENV NPM_CONFIG_LOGLEVEL error
ENV NPM_ENV production

# Install build deps
RUN apk update && apk upgrade
RUN apk --update --no-cache add --virtual build-dependencies bash git openssh  build-base gcc wget git python
# Copy all local files into the image.
COPY . .

# Build for production.
RUN set -x \
  && npm config set unsafe-perm true \
  && npm install \
  && npm run build \
  && npm install -g serve

# Remove build deps
RUN apk del build-dependencies

CMD [ "serve", "-s", "build" ]

# Tell Docker about the port we'll run on.
EXPOSE 5000
