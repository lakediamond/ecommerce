import React from 'react';

const Footer = () => {
  const linkPrefix =
    process.env.REACT_APP_LD_LINK_PREFIX || 'https://lakediamond.ch';
  return (
    <div id="footer" className="section">
      <div className="footer--second">
        <div className="container">
          <div className="row">
            <div className="footer__logo center-block">
              <img
                className="footer--logo-site img-responsive"
                src="/img/LKD.png"
                alt="footer--logo"
              />
            </div>
          </div>
          <div className="row">
            <div className="social-links--wrapper">
              <div className="social-links__twitter social-links">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://twitter.com/LakeDiamond">
                  <i className="fa fa-twitter" />
                </a>
              </div>
              <div className="social-links__linkedin social-links">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.linkedin.com/company/lakediamond/">
                  <i className="fa fa-linkedin" />
                </a>
              </div>
              <div className="social-links__medium social-links">
                <a
                  target="_blank"
                  href="https://medium.com/@LakeDiamond/"
                  rel="noopener noreferrer">
                  <i className="fa fa-medium" />
                </a>
              </div>
              <div className="social-links__youtube social-links">
                <a
                  target="_blank"
                  href="https://www.youtube.com/channel/UCax1IHyIqGPtK-XpebQAXLw">
                  <i className="fa fa-youtube" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer--third">
        <div className="container">
          <div className="row">
            <div className="col--first col-md-6 col-sm-8 col-xs-11 col-xs-offset-1 col-md-offset-0 col-sm-offset-0">
              <ul className="footer__menu">
                <li>
                  <a href={`${linkPrefix}/about-us`}>About us</a>
                </li>
                <li>
                  <a href={`${linkPrefix}/ourtechnologies`}>Our technologies</a>
                </li>
                <li>
                  <a href={`${linkPrefix}/explore`}>Our applications</a>
                </li>
                <li>
                  <a href={`${linkPrefix}/products`}>Our products</a>
                </li>
                <li>
                  <a href={`${linkPrefix}/tokensale`}>Token sale</a>
                </li>
                <li>
                  <a href="/">Shop</a>
                </li>
                <li>
                  <a href="https://medium.com/@LakeDiamond/">Blog</a>
                </li>
                <li>
                  <a href={`${linkPrefix}/contact`}>Contact</a>
                </li>
              </ul>
            </div>
            <div className="col--second col-md-6 col-sm-8 col-xs-11 col-xs-offset-1 col-md-offset-3 col-sm-offset-0">
              <div className="footer__add-1">
                <h4 className="add__title">Offices</h4>
                <div className="add__addr">
                  <a
                    href="https://www.google.fr/maps/place/EPFL+Innovation+Park/@46.5173496,6.5606222,17z/data=!3m1!4b1!4m5!3m4!1s0x478c30fc9254b463:0x90b5a0440403b2c!8m2!3d46.5173496!4d6.5628109"
                    target="_blank">
                    <span>
                      LakeDiamond SA
                      <br />
                      EPFL Innovation Park - Bldg. D<br />
                      1015 Lausanne
                      <br />
                      Switzerland
                    </span>
                  </a>
                </div>
                <div className="add__mail">
                  <a href="mailto:info@lakediamond.ch">
                    <span>info@lakediamond.ch</span>
                  </a>
                </div>
              </div>
            </div>
            <div className="col--third col-md-6 col-sm-8 col-xs-11 col-xs-offset-1 col-md-offset-3 col-sm-offset-0">
              <div className="footer__add-2">
                <h4 className="add__title">Registered address</h4>
                <div className="add__addr">
                  <a
                    href="https://www.google.fr/maps/place/Rue+Galil%C3%A9e+7,+1400+Yverdon-les-Bains,+Suisse/@46.7654042,6.6439513,17z/data=!3m1!4b1!4m13!1m7!3m6!1s0x478dcfbc3034844f:0x2c12c3713bcab093!2sRue+Galil%C3%A9e+7,+1400+Yverdon-les-Bains,+Suisse!3b1!8m2!3d46.7654005!4d6.6461453!3m4!1s0x478dcfbc3034844f:0x2c12c3713bcab093!8m2!3d46.7654005!4d6.6461453"
                    target="_blank">
                    <span>
                      LakeDiamond SA
                      <br />
                      Rue Galilée 7<br />
                      1400 Yverdon-les-Bains
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer--fourth">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <span className="terms-and-conditions">
                <a href="/terms-and-conditions">Terms &amp; conditions</a>
                &nbsp;|&nbsp;
                <a href="/disclaimer">Disclaimer</a>
              </span>
            </div>
            <div className="col-md-12">
              <span className="copyright">© 2018 Lake Diamond</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
