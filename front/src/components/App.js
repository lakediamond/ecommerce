import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Cart from './Cart/Cart';
import ProductsContainer from './Products/ProductsContainer';
import SingleProductContainer from './Products/SingleProductContainer';
import OneClickCheckout from './Checkout/OneClickCheckout';
import NotFound from './global/NotFound';
import CheckoutContainer from './Checkout/CheckoutContainer';
import OrderConfirmationContainer from './Orders/OrderConfirmationContainer';
// import MobileNav from './global/Mobile/MobileNav';
import Footer from './global/Footer';

const App = props => (
  <React.Fragment>
    {/* <MobileNav /> */}
    <Switch>
      <Route exact path="/" component={ProductsContainer} />
      <Route path="/cart" component={Cart} />
      <Route path="/product/:id" component={SingleProductContainer} />
      <Route path="/checkout" component={CheckoutContainer} />
      <Route path="/orders/:id" component={OrderConfirmationContainer} />
      <Route
        path="/one-click-checkout/:productId"
        component={OneClickCheckout}
      />
      <Route path="*" component={NotFound} />
    </Switch>

    <Footer />
  </React.Fragment>
);

export default App;
