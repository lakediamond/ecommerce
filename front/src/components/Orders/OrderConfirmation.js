import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isMetamaskInstaled, abi } from '../../metamask';
import * as api from '../../moltin';
import Web3 from 'web3';

function mapStateToProps(state) {
  return state;
}

const CONTRACT_ADDRESS = process.env.REACT_APP_CONTRACT_ADDRESS || '';
const PAYMENT_ADDRESS = process.env.REACT_APP_PAYMENT_ACCOUNT_ADDRESS || '';

class OrderConfirmation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      metamask: false,
      web3: null,
      Contract: null,
      account: null,
      amount: '',
      complete: false,
      txHash: null,
      errorMessage: 'Please install Metamask App',
      order: null,
      orderId: '',
      enable: false,
      lkdAmount: 0,
      insufficient_funds: false
    };

    this.payForOrder = this.payForOrder.bind(this);
  }

  componentDidMount() {
    const url = window.location.href;
    const orderId = url.substring(url.lastIndexOf('/') + 1);

    this.setState({
      orderId: orderId.replace(/-/g, '')
    });

    api.GetOrderWithItems(orderId).then(order => {
      this.setOrder(order);
    });

    this.setState({
      metamask: isMetamaskInstaled()
    });

    setTimeout(() => {
      console.log(this.state);
    }, 3000);

    window.scrollTo({
      top: 0
    });

    // const getTokenBalance = account => {
    //     const contractInstance = new Contract(account, TOKEN_CONTRACT);
  }

  componentDidUpdate(prevProps, prevState) {
    const { metamask, web3, account, order } = this.state;

    if (metamask !== prevState.metamask) {
      const web3 = new Web3(window.web3.currentProvider);
      this.setState({ web3: web3 });

      if (this.isNewMetamaskVersion()) {
        this.enableMetamask();
      } else {
        const acc = web3.eth.getAccounts();
        acc.then(acc => {
          return acc.length > 0 ? this.setAccount(acc.join('')) : false;
        });
      }
    }

    if (account != prevState.account) {
      setTimeout(() => {
        this.startApp(web3);
      }, 1000);
    }

    if (order != prevState.order) {
      const amount = order['data']
        ? String(order['data'].meta.display_price.with_tax['amount'])
        : null;

      this.setState({
        amount: amount
      });
    }
  }

  render() {
    const { metamask, order, insufficient_funds, rules_approved } = this.state;

    const errorContainer = metamask ? null : (
      <div>{this.state.errorMessage}</div>
    );

    const CompleteView = () => {
      const url = process.env.REACT_APP_BLOCKCHAIN_EXPLORER_BASE_URL;
      const txlink = txHash => `${url}${txHash}`;
      return (
        <div>
          <h3>
            Thank you, <b>{order['data']['customer']['name']}</b>
          </h3>
          <span>
            Your transaction (
            <a href={txlink(this.state.txHash)}>{this.state.txHash}</a>) has
            been received.
          </span>
          <span> You will get an email with confirmation of your payment!</span>
        </div>
      );
    };

    const OrderView = () => {
      return order ? (
        <div>
          <hr />
          <div className="content">
            <form className="checkout-form">
              <fieldset className="details">
                <div className="form-header">
                  <h2>Your details</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    <label className="input-wrap name ">
                      <span className="">Name</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['customer']['name']}
                      />
                    </label>
                    <label className="input-wrap email ">
                      <span className="">Email address</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['customer']['email']}
                      />
                    </label>
                  </div>
                </div>
              </fieldset>
              <fieldset className="billing">
                <div className="form-header inactive">
                  <h2>Billing address</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    <label className="input-wrap firstname">
                      <span className="">First name</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['billing_address']['first_name']}
                      />
                    </label>
                    <label className="input-wrap lastname">
                      <span className="">Last name</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['billing_address']['last_name']}
                      />
                    </label>
                    <label className="input-wrap company">
                      <span className="">Company</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['billing_address']['company']}
                      />
                    </label>
                    <label className="input-wrap address-1">
                      <span className="">Address line 1</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['billing_address']['line_1']}
                      />
                    </label>
                    <label className="input-wrap address-2">
                      <span className="">Address line 2</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['billing_address']['line_2']}
                      />
                    </label>
                    <label className="input-wrap state">
                      <span className="">State or county</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['billing_address']['country']}
                      />
                    </label>
                    <label className="input-wrap postcode">
                      <span className="">Postcode</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['billing_address']['postcode']}
                      />
                    </label>
                    <div className="input-wrap country">
                      <label className="">
                        <span className="">Country</span>
                        <input
                          className="name"
                          readOnly={true}
                          value={order['data']['billing_address']['country']}
                        />
                      </label>
                    </div>
                  </div>
                </div>
              </fieldset>
              {/* <fieldset className="shipping">
                <div className="form-header inactive">
                  <h2>Shipping address</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    <label className="input-wrap firstname ">
                      <span className="">First name</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['shipping_address']['first_name']}
                      />
                    </label>
                    <label className="input-wrap lastname ">
                      <span className="">Last name</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['shipping_address']['last_name']}
                      />
                    </label>
                    <label className="input-wrap company">
                      <span className="">Company</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['shipping_address']['company']}
                      />
                    </label>
                    <label className="input-wrap address-1 ">
                      <span className="">Address line 1</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['shipping_address']['line_1']}
                      />
                    </label>
                    <label className="input-wrap address-2">
                      <span className="">Address line 2</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['shipping_address']['line_2']}
                      />
                    </label>
                    <label className="input-wrap state ">
                      <span className="">State or county</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['shipping_address']['country']}
                      />
                    </label>
                    <label className="input-wrap postcode ">
                      <span className="">Postcode</span>
                      <input
                        className="name"
                        readOnly={true}
                        value={order['data']['shipping_address']['postcode']}
                      />
                    </label>
                    <div className="input-wrap country">
                      <label className="">
                        <span className="">Country</span>
                        <input
                          className="name"
                          readOnly={true}
                          value={order['data']['shipping_address']['country']}
                        />
                      </label>
                    </div>
                  </div>
                </div>
              </fieldset> */}
            </form>
            <div className="checkout-summary">
              {errorContainer}
              {this.state.metamask && !this.state.account && (
                <p>Please login to metamask and refresh the page</p>
              )}
              <button
                disabled={!this.state.metamask || insufficient_funds}
                onClick={e => this.payForOrder(e)}
                className="center">
                {insufficient_funds
                  ? 'Your tokens balance is not enough to proceed'
                  : 'Pay with Metamask'}
              </button>
            </div>
          </div>
        </div>
      ) : (
        <a href="/products">
          You did not create an order, please return to product page
        </a>
      );
    };

    return (
      <main role="main" id="container" className="main-container push">
        <section className="checkout">
          <div className="content">
            {this.state.complete ? CompleteView() : OrderView()}
          </div>
        </section>
      </main>
    );
  }

  setAccount(account) {
    this.setState({ account: account });
  }

  setOrder(order) {
    this.setState({ order: order });
  }

  isNewMetamaskVersion = () => !!window.ethereum;

  enableMetamask = async () => {
    try {
      const acc = await window.ethereum.enable();
      return this.setAccount(acc.length > 0 ? acc.join('') : false);
    } catch (e) {
      return false;
    }
  };

  completePayment() {
    this.setState({ complete: true });
    this.updateOrder();
  }

  setTxHash(txHash) {
    this.setState({ txHash: txHash });
  }

  startApp = web3 => {
    const web3js = new Web3(window.web3.currentProvider);
    this.getNetId();
    const Contract = new web3js.eth.Contract(abi, CONTRACT_ADDRESS);
    Contract.methods
      .balanceOf(this.state.account)
      .call({ from: CONTRACT_ADDRESS }, (err, res) => {
        if (!err) {
          return this.setState({
            lkdAmount: res,
            insufficient_funds: Number(this.state.amount) >= Number(res)
          });
        } else {
          console.log(err);
          this.setState({ insufficient_funds: true });
        }
      });
    this.setState({ Contract: Contract });
  };

  transfer = () => {
    const { orderId, web3, amount, account } = this.state;

    const encode = web3.eth.abi.encodeParameter('uint256', amount).substring(2);

    const data = String(
      'a9059cbb' +
        '000000000000000000000000' +
        PAYMENT_ADDRESS.substring(2) +
        encode +
        orderId
    ).toLowerCase();

    const from = this.state.account;
    const self = this;

    web3.eth.sendTransaction(
      {
        from: from,
        to: CONTRACT_ADDRESS,
        data: data
      },
      function(err, result) {
        if (!err) {
          self.setTxHash(result.toString());
          self.completePayment();
        } else {
          console.log(err);
        }
      }
    );
  };

  getNetId = () => {
    const { web3 } = this.state;

    web3.eth.net.getNetworkType((err, netId) => {
      if (netId === undefined) {
        this.getNetId();
      }
    });
  };

  payForOrder(e) {
    e.preventDefault();
    const { account } = this.state;

    if (!account) {
      alert('Please login to metamask and refresh the page');
      return false;
    }
    this.transfer();
  }

  updateOrder = () => {
    const { txHash } = this.state;
    const orderId = this.state.order['data']['id'];
    const data = {
      order_repayment_transaction_hash: `${txHash}`
    };

    api.UpdateOrder(orderId, data);
  };
}

// export default OrderConfirmation;
export default connect(mapStateToProps)(OrderConfirmation);
