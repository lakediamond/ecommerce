import React from 'react';
import TopPicksContainer from './TopPicksContainer';

const HomeMainSection = () => (
  <main role="main" id="container" className="main-container push">
    <TopPicksContainer />
  </main>
);

export default HomeMainSection;
