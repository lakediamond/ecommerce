import React, { Component } from 'react';
import CheckoutSummary from './CheckoutSummary';
import { Field, reduxForm } from 'redux-form';
import * as api from '../../moltin';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import { FETCH_ORDER_END } from '../../ducks/orders';
import { SUBMIT_PAYMENT, PAYMENT_COMPLETE } from '../../ducks/payments';

function mapStateToProps(state) {
  return { cart: state.cart };
}

class CheckoutForm extends Component {
  state = {
    paymentMethod: 'lkd',
    showDialog: false
  };

  handleKeyDown = function(e) {
    if (e.key === 'Enter' && e.shiftKey === false) {
      e.preventDefault();
    }
  };

  mySubmit = values => {
    const customer = {
      email: `${values.email}`,
      name: `${values.name}`
    };

    const billing = {
      first_name: `${values.billing_firstname}`,
      last_name: `${values.billing_lastname}`,
      line_1: `${values.billing_address_1}`,
      line_2: values.billing_address_2 ? `${values.billing_address_2}` : '',
      county: `${values.billing_state}`,
      postcode: `${values.billing_postcode}`,
      country: `${values.billing_country}`
    };

    this.props.dispatch(dispatch => {
      dispatch({ type: SUBMIT_PAYMENT });
    });

    api
      .Checkout(customer, billing)

      .then(order => {
        this.props.dispatch(dispatch => {
          dispatch({ type: FETCH_ORDER_END, payload: order });
          dispatch(push(`/orders/${order.data.id}`));
        });
        api.DeleteCart();
      })

      .catch(e => {
        console.log(e);
      })

      .catch(e => {
        console.log(e);
      })

      .catch(e => {
        console.log(e);
      });
  };

  changePaymentMethod = e => {
    this.setState({ paymentMethod: e.currentTarget.id });
  };

  openDialog = () => {
    const body = document.getElementById('body');
    body.classList.add('dialog_opened');
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    const body = document.getElementById('body');
    body.classList.remove('dialog_opened');
    this.setState({ showDialog: false });
  };

  render() {
    const { paymentMethod } = this.state;
    const {
      cart: { cart }
    } = this.props;
    // const isRoundDiamondInCart =
    //   (cart &&
    //     cart.data &&
    //     Array.isArray(cart.data) &&
    //     Boolean(
    //       cart.data.find(item => item.name.toLowerCase().includes('brillant'))
    //     )) ||
    //   false;

    return (
      <main role="main" id="container" className="main-container push">
        <section className="checkout">
          {this.state.showDialog ? (
            <div className={'dialog'}>
              <div className={'dialog__content'}>
                <h3 className={'dialog__title'}>
                  GENERAL CONDITIONS OF SALE AND DELIVERY
                </h3>
                <p>
                  <h2>SCOPE</h2>
                  <div>
                    The present general conditions of sale and delivery (the
                    "Conditions") govern the contractual relationship between
                    LakeDiamond SA ("LD") and clients willing to buy any LD's
                    products (the "Client"). By purchasing any of LD's product
                    (the "Product"), the Client accepts these Conditions, as
                    amended from time to time, as well as LD's Privacy Policy,
                    available online via the following link:
                    https://medium.com/lakediamond/lakediamond-documents-37ef6e21932d.
                    These Conditions may be subject to additional ad hoc sale
                    agreement(s) entered into between a Client and LD, if any,
                    in order to accommodate a Client's specific needs. In that
                    case, such agreement shall prevail over these Conditions
                    should they contradict each other.
                  </div>
                </p>
                <p>
                  <h2>PRODUCTS</h2>
                  <div>
                    Products are produced by LD in manufacturing sites located
                    in Switzerland.
                  </div>
                  <div>
                    LD offers Clients the possibility to purchase, either
                    through its e-commerce online website at:
                    https://lakediamond.ch/products (the "Website") or via
                    direct sale, any of LD's Products.
                  </div>
                  <div>
                    The Client shall have access to all necessary information
                    about a Product either via LD's Website or by contacting LD
                    directly.
                  </div>
                </p>
                <p>
                  <h2>QUOTATIONS</h2>
                  Direct Sale for special projects Product's prices are set-up
                  according to the Client's need. In order to obtain a
                  quotation, the Client must contact LD by any mean of
                  communication (for instance: email, telephone, post or in
                  personam). During this phase, LD shall inform the Client,
                  among other things, about the description and specifications
                  of any relevant Products. Once the Products have been chosen
                  by the Client and the choice communicated to LD, the latter
                  will submit to the Client a quotation for the relevant
                  quantity and type of Products the Client is willing to buy
                  (the "Quotation"), together with these Conditions. The
                  Quotation shall also indicate an estimate of delivery.
                  Quotations will be made available to the client by email or
                  any other agreed communications' channels. Unless otherwise
                  stated therein, Quotations are valid for 30 days as from the
                  date it is sent, to the Client. A new Quotation, subject to
                  market prices changes, must then be requested by the Client.
                  LD's Quotations are confidential and only the recipient of the
                  Quotation must have access to it. LD reserves its copyright on
                  all plans, projects, diagrams and drawings presented with the
                  Quotation, if any. Clients shall not make them available to
                  third parties or copy them without LD's prior written
                  permission. These documents must be returned and/or destroyed
                  at LD's request. E-commerce Website Product's prices are
                  displayed on the Website. Should a Client would like to obtain
                  more information about a specific Product or make an offer,
                  the Client shall directly contact LD as indicated under clause
                  3.1. PRICES Unless provided otherwise, prices are VAT excluded
                  and stated in Swiss francs. LD reserves its right to adapt the
                  prices at any time in the event of an increase of prices of
                  its suppliers, tax and customs taxes, or of any other major
                  monetary change. In such case, the Client shall have the right
                  to cancel an order.
                </p>
                <p>
                  <h2>PAYMENT AND PRODUCTION</h2>
                  Direct sale Acceptance of a Quotation must be communicated to
                  LD by email, post, or any other written mean. LD shall then
                  issue an invoice to be paid by the Client, stating the Price
                  and the payment's terms and conditions. Except otherwise
                  provided in the Quotation or agreed with the Client, invoices
                  are to be settled net, in full, and without any deduction,
                  within 30 days of receipt. In the event the Client does not
                  pay the invoice within the time limit, LD shall issue a
                  reminder to Client and extend the validity of the invoice by
                  15 days. In case the Client still not pays the invoice, the
                  Quotation shall be automatically withdrawn. Once the payment
                  has been credited to LD's bank account, LD shall issue a
                  payment confirmation to the Client and thereafter start
                  manufacturing the Products. E-commerce Website Except
                  otherwise agreed with the Client, in the event a Client wishes
                  to purchase a Product via the Website, it must pay the full
                  amount corresponding to its order. Before proceeding to
                  payment, the Client shall have the possibility to accept these
                  Conditions. The Client shall however not be entitled to
                  purchase any Product should it refuse to accept these
                  Conditions, unless otherwise stated in a separate agreement.
                  Once the payment has been made, LD shall issue a payment
                  confirmation to the Client by email and thereafter start
                  manufacturing the Products. MEANS OF PAYMENT A Client shall be
                  able to pay for any of LD's Products either with LakeDiamond
                  Tokens, by credit cards or by wire transfer to the following
                  bank account: Bank: Swissquote Bank SA Swift: SWQBCHZZXX
                  Beneficiary: LakeDiamond SA Rue Galilée 7 1400
                  Yverdon-les-Bains, Switzerland IBAN: CH0708781000108630600
                  ORDER CANCELLATION AND REFUND The Client has the right to
                  cancel an order as long as LD has not started manufacturing
                  the corresponding Products. If the manufacturing process has
                  started, the transaction can no longer be cancelled and the
                  purchase price will not be refunded to the Client. For the
                  avoidance of doubt, the manufacturing process shall in any
                  case only start once payment has been made by the Client to
                  LD. DELIVERY The estimated time for a delivery to take place
                  will be communicated by email to the Client once a payment has
                  been credited to LD. Such estimation will vary depending on
                  the quantity and specificity of the Products. LD always
                  undertakes, on a best effort basis, to comply with delivery
                  estimates. However, LD does not represent nor guarantee that
                  Products will always be delivered in accordance with its
                  estimation. In the event of a delay, the Client is neither
                  entitled to cancel the order nor claim any payment for any
                  damages, whatsoever. As such, LD shall not be liable for any
                  damages caused by a delay. All delivery will be operated with
                  a trusted and reliable logistic company such as: DHL, UPS,
                  Brinks, FedEx, etc. Their respective relevant terms and
                  conditions shall therefore apply in relation with a delivery.
                  Costs incurred for the delivery such as the handling and
                  packing, insurance and transport, are at the Client expense.
                  Alternatively, the Client may choose to collect in personam
                  the Products once available, should the Client choose to. It
                  is the Client responsibility to contact LD in order to
                  organize for the pick-up of the Products. CUSTOMS TAXES AND
                  OTHER TAXES The Client shall incur all applicable customs
                  taxes, if applicable, and VAT on Product prices. WARRANTY LD's
                  warranty will extend to all defects resulting from poor
                  quality material or defect resulting from the conception of
                  the Product. However, the warranty does not include defects
                  resulting from the common usage of the Product, lack of care,
                  failure to comply with any instruction relating to the Product
                  (if any), inappropriate use and operating methods, and any
                  other causes not directly or indirectly attributable to LD.
                  The warranty allows the customer to either request that the
                  Product be replaced or that it be refunded in full, in
                  exchange for the defective Product. The choice to either
                  refund the Client or replace the Product is at the sole
                  discretion of LD, depending of the circumstances of the case.
                  In order to trigger the warranty, the Client shall contact LD
                  in accordance with clause 11 below, in order to set up the
                  procedure to be followed, which will vary depending on the
                  circumstances of the case. In any event, the defective parts
                  and/or Product shall be returned to LD's premises for
                  inspection. LD assumes no liability for modifications or
                  repairs made to a Product or parts of it which have not been
                  carried out by LD's personnel or by specialists appointed by
                  LD. In such event, the warranty shall cease to operate. The
                  Client shall also take appropriate measures to ensure that the
                  defect of the Product or any of its part is not aggravated.
                  The warranty shall cease should the Client fail to act
                  accordingly. LD shall also not be liable, in particular, for
                  resulting damages.
                </p>
                <p>
                  <h2>CLAIMS AND RETURN</h2>
                  The Client agrees to inspect the Products on delivery and to
                  notify any apparent defect to LD via email at the relevant
                  email address indicated on the Quotation within 5 working
                  days; the same applies to possible hidden defects which only
                  appear afterward. Otherwise the delivery is considered to be
                  accepted. EXPORT AND RE-EXPORT Our Product deliveries are
                  intended to be used in all countries, persons or entities
                  listed on the State Secretariat for Economic Affairs' website
                  (SECO), as amended from time to time. PLACE OF PERFORMANCE The
                  place of performance for the sale of LD's Products shall be
                  Yverdon-Les-Bains
                </p>
                <p>
                  <h2>CONFIDENTIALITY</h2>
                  Throughout the duration of the manufacturing and delivery
                  process of the Products under these Conditions and following
                  its termination, the Clients and LD shall treat as
                  confidential any information disclosed to them in providing
                  and receiving services and Products under these Conditions,
                  including, but not limited to trade secrets, personal data and
                  know-how. This duty of confidentiality shall not apply to: any
                  information that is in the public domain, the disclosure of
                  which has been made otherwise than by breach of these
                  Conditions; any information which is disclosed by written
                  consent of the party entitled to confidentiality; and any
                  information which is already known to either party other than
                  by reason of the services and manufacturing process rendered
                  under these Conditions. Notwithstanding the provisions of this
                  clause, the Client and LD shall be entitled to disclose any
                  information required to be disclosed by reason of any
                  statutory or regulatory provision, any decision by a court or
                  public authority, any professional rules or rules regarding
                  independent status, or to safeguard their interests in
                  relation to their insurers and legal advisors. LD may further
                  disclose any information subject to a statutory or contractual
                  duty of non-disclosure to any third parties sub-contracted to
                  perform any services and deliver Product under these
                  Conditions on a need to know basis.
                </p>
                <p>
                  <h2>FORCE MAJEURE</h2>A force majeure event means by reason of
                  force majeure or act of state occurring, on any day LD is
                  prevented from performing any absolute or contingent
                  obligation to make a delivery and/or a payment and/or from
                  receiving a payment and/or a delivery under these Conditions,
                  or from complying with any other material provision of these
                  Conditions or any other specific agreement, or it becomes
                  impossible or impracticable for LD so to perform, receive or
                  comply, so long as the force majeure or act of state is beyond
                  the control of LD and LD could not, after using all reasonable
                  efforts (which will not require LD to incur a loss, other than
                  immaterial, incidental expenses), overcome such prevention,
                  impossibility or impracticality. The following events shall,
                  among others, constitute force majeure events: power failure,
                  earthquake, impossibility to have access to a specific
                  supplier, war, economic crisis, and any other circumstances
                  beyond the reasonable control of LD. RETENTION OF TITLE AND
                  TRANSFER OF RISKS LD reserves all its property right on all
                  Products until receipt by the Client of such Products. Risk of
                  loss or damage to the Products, and responsibility for, shall
                  pass to the Client upon shipment of the Products to the Client
                  or submission to the logistic company. INTELLECTUAL PROPERTY
                  LD shall own all right, title and interest in all Products,
                  including but not limited to intellectual property rights, if
                  any, and in any know-how developed or used by LD,
                  notwithstanding any assistance afforded by or involvement on
                  the part of the Client. LD shall be entitled to develop any
                  know-how acquired in providing the Products and services (such
                  as any technology, ideas, concepts, methods, processes,
                  programs, or interfaces), or to use such know-how for the
                  further development of its Products and services.
                </p>
                <p>
                  <h2>GOVERNING LAW AND JURISDICTION</h2>
                  These Conditions connected with it shall be governed by and
                  construed in accordance with the laws of Switzerland
                  (excluding the Swiss Private International Law Act and
                  international treaties, in particular the Vienna Convention on
                  the International Sale of Goods dated April 11, 1980). All
                  disputes arising out of or in connection with these
                  Conditions, including disputes on its conclusion, binding
                  effect, amendment and termination, shall be resolved by the
                  ordinary Courts of the Canton of Vaud. On a best effort basis,
                  both the Client and LD shall first try to settle any issues
                  amicably before engaging any legal proceedings.
                  Yverdon-les-Bains, January 2019
                </p>
              </div>
              <button
                type="button"
                className="continue"
                onClick={() => this.closeDialog()}>
                Continue
              </button>
            </div>
          ) : null}
          <div className="content">
            <CheckoutSummary />
            <form
              className="checkout-form"
              noValidate
              onSubmit={this.props.handleSubmit(this.mySubmit)}
              onKeyDown={e => {
                this.handleKeyDown(e);
              }}>
              <fieldset className="details">
                <div className="form-header">
                  <h2>Your details</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    <label className="input-wrap name required">
                      <span className="hide-content">Name</span>
                      <Field
                        component="input"
                        className="name"
                        required="required"
                        placeholder="Name"
                        name="name"
                        type="text"
                        aria-label="Name"
                      />
                    </label>
                    <label className="input-wrap email required">
                      <span className="hide-content">Email address</span>
                      <Field
                        component="input"
                        className="email"
                        required="required"
                        placeholder="Email address"
                        name="email"
                        type="email"
                        aria-label="Email"
                      />
                    </label>
                  </div>
                  <button type="button" className="continue">
                    Continue
                  </button>
                </div>
              </fieldset>
              <fieldset className="billing collapsed">
                <div className="form-header inactive">
                  <h2>Billing address</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    <label className="input-wrap firstname required">
                      <span className="hide-content">First name</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="First Name"
                        name="billing_firstname"
                        type="text"
                        aria-label="First name"
                      />
                    </label>
                    <label className="input-wrap lastname required">
                      <span className="hide-content">Last name</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Last Name"
                        name="billing_lastname"
                        type="text"
                        aria-label="Last name"
                      />
                    </label>
                    <label className="input-wrap company">
                      <span className="hide-content">Company</span>
                      <Field
                        component="input"
                        placeholder="Company"
                        name="billing-company"
                        type="text"
                        aria-label="Company"
                      />
                    </label>
                    <label className="input-wrap address-1 required">
                      <span className="hide-content">Address line 1</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Address Line 1"
                        name="billing_address_1"
                        type="text"
                        aria-label="Address line 1"
                      />
                    </label>
                    <label className="input-wrap address-2">
                      <span className="hide-content">Address line 2</span>
                      <Field
                        component="input"
                        placeholder="Address Line 2"
                        name="billing_address_2"
                        type="text"
                        aria-label="Address line 2"
                      />
                    </label>
                    <label className="input-wrap state required">
                      <span className="hide-content">State or county</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="State / County"
                        name="billing_state"
                        type="text"
                        aria-label="State / County"
                      />
                    </label>
                    <label className="input-wrap postcode required">
                      <span className="hide-content">Postcode</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Postcode"
                        name="billing_postcode"
                        type="text"
                        aria-label="Postcode"
                      />
                    </label>
                    <div className="input-wrap country">
                      <label className="required select-fallback">
                        <span className="hide-content">Country</span>
                        <Field
                          component="select"
                          id="billing_country"
                          required="required"
                          name="billing_country">
                          <option value="">Country</option>
                          <option value="AF">Afghanistan</option>
                          <option value="AX">Åland Islands</option>
                          <option value="AL">Albania</option>
                          <option value="DZ">Algeria</option>
                          <option value="AS">American Samoa</option>
                          <option value="AD">Andorra</option>
                          <option value="AO">Angola</option>
                          <option value="AI">Anguilla</option>
                          <option value="AQ">Antarctica</option>
                          <option value="AG">Antigua and Barbuda</option>
                          <option value="AR">Argentina</option>
                          <option value="AM">Armenia</option>
                          <option value="AW">Aruba</option>
                          <option value="AU">Australia</option>
                          <option value="AT">Austria</option>
                          <option value="AZ">Azerbaijan</option>
                          <option value="BS">Bahamas</option>
                          <option value="BH">Bahrain</option>
                          <option value="BD">Bangladesh</option>
                          <option value="BB">Barbados</option>
                          <option value="BY">Belarus</option>
                          <option value="BE">Belgium</option>
                          <option value="BZ">Belize</option>
                          <option value="BJ">Benin</option>
                          <option value="BM">Bermuda</option>
                          <option value="BT">Bhutan</option>
                          <option value="BO">
                            Bolivia, Plurinational State of
                          </option>
                          <option value="BQ">
                            Bonaire, Sint Eustatius and Saba
                          </option>
                          <option value="BA">Bosnia and Herzegovina</option>
                          <option value="BW">Botswana</option>
                          <option value="BV">Bouvet Island</option>
                          <option value="BR">Brazil</option>
                          <option value="IO">
                            British Indian Ocean Territory
                          </option>
                          <option value="BN">Brunei Darussalam</option>
                          <option value="BG">Bulgaria</option>
                          <option value="BF">Burkina Faso</option>
                          <option value="BI">Burundi</option>
                          <option value="KH">Cambodia</option>
                          <option value="CM">Cameroon</option>
                          <option value="CA">Canada</option>
                          <option value="CV">Cape Verde</option>
                          <option value="KY">Cayman Islands</option>
                          <option value="CF">Central African Republic</option>
                          <option value="TD">Chad</option>
                          <option value="CL">Chile</option>
                          <option value="CN">China</option>
                          <option value="CX">Christmas Island</option>
                          <option value="CC">Cocos (Keeling) Islands</option>
                          <option value="CO">Colombia</option>
                          <option value="KM">Comoros</option>
                          <option value="CG">Congo</option>
                          <option value="CD">
                            Congo, the Democratic Republic of the
                          </option>
                          <option value="CK">Cook Islands</option>
                          <option value="CR">Costa Rica</option>
                          <option value="CI">Côte d'Ivoire</option>
                          <option value="HR">Croatia</option>
                          <option value="CU">Cuba</option>
                          <option value="CW">Curaçao</option>
                          <option value="CY">Cyprus</option>
                          <option value="CZ">Czech Republic</option>
                          <option value="DK">Denmark</option>
                          <option value="DJ">Djibouti</option>
                          <option value="DM">Dominica</option>
                          <option value="DO">Dominican Republic</option>
                          <option value="EC">Ecuador</option>
                          <option value="EG">Egypt</option>
                          <option value="SV">El Salvador</option>
                          <option value="GQ">Equatorial Guinea</option>
                          <option value="ER">Eritrea</option>
                          <option value="EE">Estonia</option>
                          <option value="ET">Ethiopia</option>
                          <option value="FK">
                            Falkland Islands (Malvinas)
                          </option>
                          <option value="FO">Faroe Islands</option>
                          <option value="FJ">Fiji</option>
                          <option value="FI">Finland</option>
                          <option value="FR">France</option>
                          <option value="GF">French Guiana</option>
                          <option value="PF">French Polynesia</option>
                          <option value="TF">
                            French Southern Territories
                          </option>
                          <option value="GA">Gabon</option>
                          <option value="GM">Gambia</option>
                          <option value="GE">Georgia</option>
                          <option value="DE">Germany</option>
                          <option value="GH">Ghana</option>
                          <option value="GI">Gibraltar</option>
                          <option value="GR">Greece</option>
                          <option value="GL">Greenland</option>
                          <option value="GD">Grenada</option>
                          <option value="GP">Guadeloupe</option>
                          <option value="GU">Guam</option>
                          <option value="GT">Guatemala</option>
                          <option value="GG">Guernsey</option>
                          <option value="GN">Guinea</option>
                          <option value="GW">Guinea-Bissau</option>
                          <option value="GY">Guyana</option>
                          <option value="HT">Haiti</option>
                          <option value="HM">
                            Heard Island and McDonald Islands
                          </option>
                          <option value="VA">
                            Holy See (Vatican City State)
                          </option>
                          <option value="HN">Honduras</option>
                          <option value="HK">Hong Kong</option>
                          <option value="HU">Hungary</option>
                          <option value="IS">Iceland</option>
                          <option value="IN">India</option>
                          <option value="ID">Indonesia</option>
                          <option value="IR">Iran, Islamic Republic of</option>
                          <option value="IQ">Iraq</option>
                          <option value="IE">Ireland</option>
                          <option value="IM">Isle of Man</option>
                          <option value="IL">Israel</option>
                          <option value="IT">Italy</option>
                          <option value="JM">Jamaica</option>
                          <option value="JP">Japan</option>
                          <option value="JE">Jersey</option>
                          <option value="JO">Jordan</option>
                          <option value="KZ">Kazakhstan</option>
                          <option value="KE">Kenya</option>
                          <option value="KI">Kiribati</option>
                          <option value="KP">
                            Korea, Democratic People's Republic of
                          </option>
                          <option value="KR">Korea, Republic of</option>
                          <option value="KW">Kuwait</option>
                          <option value="KG">Kyrgyzstan</option>
                          <option value="LA">
                            Lao People's Democratic Republic
                          </option>
                          <option value="LV">Latvia</option>
                          <option value="LB">Lebanon</option>
                          <option value="LS">Lesotho</option>
                          <option value="LR">Liberia</option>
                          <option value="LY">Libya</option>
                          <option value="LI">Liechtenstein</option>
                          <option value="LT">Lithuania</option>
                          <option value="LU">Luxembourg</option>
                          <option value="MO">Macao</option>
                          <option value="MK">
                            Macedonia, the former Yugoslav Republic of
                          </option>
                          <option value="MG">Madagascar</option>
                          <option value="MW">Malawi</option>
                          <option value="MY">Malaysia</option>
                          <option value="MV">Maldives</option>
                          <option value="ML">Mali</option>
                          <option value="MT">Malta</option>
                          <option value="MH">Marshall Islands</option>
                          <option value="MQ">Martinique</option>
                          <option value="MR">Mauritania</option>
                          <option value="MU">Mauritius</option>
                          <option value="YT">Mayotte</option>
                          <option value="MX">Mexico</option>
                          <option value="FM">
                            Micronesia, Federated States of
                          </option>
                          <option value="MD">Moldova, Republic of</option>
                          <option value="MC">Monaco</option>
                          <option value="MN">Mongolia</option>
                          <option value="ME">Montenegro</option>
                          <option value="MS">Montserrat</option>
                          <option value="MA">Morocco</option>
                          <option value="MZ">Mozambique</option>
                          <option value="MM">Myanmar</option>
                          <option value="NA">Namibia</option>
                          <option value="NR">Nauru</option>
                          <option value="NP">Nepal</option>
                          <option value="NL">Netherlands</option>
                          <option value="NC">New Caledonia</option>
                          <option value="NZ">New Zealand</option>
                          <option value="NI">Nicaragua</option>
                          <option value="NE">Niger</option>
                          <option value="NG">Nigeria</option>
                          <option value="NU">Niue</option>
                          <option value="NF">Norfolk Island</option>
                          <option value="MP">Northern Mariana Islands</option>
                          <option value="NO">Norway</option>
                          <option value="OM">Oman</option>
                          <option value="PK">Pakistan</option>
                          <option value="PW">Palau</option>
                          <option value="PS">
                            Palestinian Territory, Occupied
                          </option>
                          <option value="PA">Panama</option>
                          <option value="PG">Papua New Guinea</option>
                          <option value="PY">Paraguay</option>
                          <option value="PE">Peru</option>
                          <option value="PH">Philippines</option>
                          <option value="PN">Pitcairn</option>
                          <option value="PL">Poland</option>
                          <option value="PT">Portugal</option>
                          <option value="PR">Puerto Rico</option>
                          <option value="QA">Qatar</option>
                          <option value="RE">Réunion</option>
                          <option value="RO">Romania</option>
                          <option value="RU">Russian Federation</option>
                          <option value="RW">Rwanda</option>
                          <option value="BL">Saint Barthélemy</option>
                          <option value="SH">
                            Saint Helena, Ascension and Tristan da Cunha
                          </option>
                          <option value="KN">Saint Kitts and Nevis</option>
                          <option value="LC">Saint Lucia</option>
                          <option value="MF">Saint Martin (French part)</option>
                          <option value="PM">Saint Pierre and Miquelon</option>
                          <option value="VC">
                            Saint Vincent and the Grenadines
                          </option>
                          <option value="WS">Samoa</option>
                          <option value="SM">San Marino</option>
                          <option value="ST">Sao Tome and Principe</option>
                          <option value="SA">Saudi Arabia</option>
                          <option value="SN">Senegal</option>
                          <option value="RS">Serbia</option>
                          <option value="SC">Seychelles</option>
                          <option value="SL">Sierra Leone</option>
                          <option value="SG">Singapore</option>
                          <option value="SX">Sint Maarten (Dutch part)</option>
                          <option value="SK">Slovakia</option>
                          <option value="SI">Slovenia</option>
                          <option value="SB">Solomon Islands</option>
                          <option value="SO">Somalia</option>
                          <option value="ZA">South Africa</option>
                          <option value="GS">
                            South Georgia and the South Sandwich Islands
                          </option>
                          <option value="SS">South Sudan</option>
                          <option value="ES">Spain</option>
                          <option value="LK">Sri Lanka</option>
                          <option value="SD">Sudan</option>
                          <option value="SR">Suriname</option>
                          <option value="SJ">Svalbard and Jan Mayen</option>
                          <option value="SZ">Swaziland</option>
                          <option value="SE">Sweden</option>
                          <option value="CH">Switzerland</option>
                          <option value="SY">Syrian Arab Republic</option>
                          <option value="TW">Taiwan, Province of China</option>
                          <option value="TJ">Tajikistan</option>
                          <option value="TZ">
                            Tanzania, United Republic of
                          </option>
                          <option value="TH">Thailand</option>
                          <option value="TL">Timor-Leste</option>
                          <option value="TG">Togo</option>
                          <option value="TK">Tokelau</option>
                          <option value="TO">Tonga</option>
                          <option value="TT">Trinidad and Tobago</option>
                          <option value="TN">Tunisia</option>
                          <option value="TR">Turkey</option>
                          <option value="TM">Turkmenistan</option>
                          <option value="TC">Turks and Caicos Islands</option>
                          <option value="TV">Tuvalu</option>
                          <option value="UG">Uganda</option>
                          <option value="UA">Ukraine</option>
                          <option value="AE">United Arab Emirates</option>
                          <option value="GB">United Kingdom</option>
                          <option value="US">United States</option>
                          <option value="UM">
                            United States Minor Outlying Islands
                          </option>
                          <option value="UY">Uruguay</option>
                          <option value="UZ">Uzbekistan</option>
                          <option value="VU">Vanuatu</option>
                          <option value="VE">
                            Venezuela, Bolivarian Republic of
                          </option>
                          <option value="VN">Viet Nam</option>
                          <option value="VG">Virgin Islands, British</option>
                          <option value="VI">Virgin Islands, U.S.</option>
                          <option value="WF">Wallis and Futuna</option>
                          <option value="EH">Western Sahara</option>
                          <option value="YE">Yemen</option>
                          <option value="ZM">Zambia</option>
                          <option value="ZW">Zimbabwe</option>
                        </Field>
                      </label>
                    </div>
                  </div>
                  <button
                    type="button"
                    className="continue"
                    onClick={() => this.openDialog()}>
                    Continue
                  </button>
                </div>
              </fieldset>
              <fieldset className="payment collapsed">
                <div className="form-header inactive">
                  <h2>Payment details</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    {paymentMethod === 'lkd' && (
                      <div>
                        <div className="payment-info">
                          <p className="payment-info__head">
                            <div>
                              <strong>
                                This shop supports only payment with LKD tokens
                              </strong>
                            </div>
                            {/*This ecommerce now supports purchases with “mainnet”*/}
                            {/*LKD tokens.*/}
                          </p>
                          <p className="payment-info__small">
                            To make a payment using LKD tokens: <br />
                            1. Follow this{' '}
                            <a
                              href={
                                'https://bitfalls.com/2018/02/16/metamask-send-receive-ether'
                              }>
                              link
                            </a>{' '}
                            to install metamask plugin for Google Chrome or
                            Firefox <br />
                            2. Connect your existing ethereum wallet with
                            Metamask or create a new one
                            <br />
                            3. Verify you have enough LKD tokens and ETH in your
                            metamask account
                            <br />
                            4. Click the button below to pay with LKD tokens
                          </p>
                        </div>

                        <button className="pay" aria-live="polite">
                          <div className="loading-icon">
                            <span className="hide-content">Processing</span>
                          </div>
                          <span className="copy">Pay with LKD</span>
                        </button>
                      </div>
                    )}
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </section>
      </main>
    );
  }
}

CheckoutForm = reduxForm({
  form: 'CheckoutForm'
})(CheckoutForm);

export default connect(mapStateToProps)(CheckoutForm);
