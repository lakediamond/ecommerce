import React, { Component } from 'react';
import Categories from './Categories';

const styles = {
  textAlign: 'center',
  fontSize: '3rem',
  lineHeight: '3.625rem'
};
class CategoriesContainer extends Component {
  render() {
    return (
      <div className="styles">
        <div className="content" style={{ textAlign: 'center' }}>
          <h2 style={{ fontSize: '3rem', lineHeight: '3.625rem' }}>
            Our categories
          </h2>
          <Categories />
        </div>
      </div>
    );
  }
}

export default CategoriesContainer;
