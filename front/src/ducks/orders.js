export const FETCH_ORDER_END = 'orders/FETCH_ORDER_END';

const initialState = {
  order: {},
  complete: false,
  error: null,
  PaymentAccountAddress: process.env.REACT_APP_PAYMENT_ACCOUNT_ADDRESS || ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ORDER_END:
      return { ...state, complete: true, order: action.payload };

    default:
      return { ...state };
  }
};

export const fetchOrderEnd = order => ({
  type: FETCH_ORDER_END,
  payload: order
});

const returnOrderInfo = order => dispatch => {
  return dispatch(fetchOrderEnd(order));
};
